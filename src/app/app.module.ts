import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipeAddComponent } from './recipe-add/recipe-add.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

const appRoutes: Routes = [{
  path: 'recipes',
  component: RecipeComponent,
  data: { title: 'Recipes'}
  },
  {
    path: 'recipe-details/:id',
    component: RecipeDetailsComponent,
    data: { title: 'Recipe'}
  }, {
    path: 'recipe-add/',
    component: RecipeAddComponent,
    data: { title: 'Add Recipe'}
  },
  {
    path: 'recipe-edit/:id',
    component: RecipeEditComponent,
    data: { title: 'Edit Recipe'}
  }, {
    path: '',
    redirectTo: '/recipes',
    pathMatch: 'full'
  }
  ];
@NgModule({
  declarations: [
    AppComponent,
    RecipeComponent,
    RecipeAddComponent,
    RecipeEditComponent,
    RecipeDetailsComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
