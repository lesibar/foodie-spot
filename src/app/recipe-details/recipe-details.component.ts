import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendService} from '../service/backend.service';

@Component({
  selector: 'fs-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.scss']
})
export class RecipeDetailsComponent implements OnInit {

  recipe: any;
  constructor(private router: Router, private restService: BackendService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getRecipe(this.route.snapshot.params['id']);
  }

  getRecipe(id) {
    this.restService.getRecipe(id).subscribe((data: {}) => {
      this.recipe = data;
    });
  }
}
