import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable, of} from 'rxjs/index';
import { map, catchError, tap } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  constructor(private httpClient: HttpClient) { }

  getRecipes(): Observable<any> {
    return this.httpClient.get(environment.serverUrl + 'recipes').pipe(map(this.extractData));
  }

  getRecipe(id): Observable<any> {
    return this.httpClient.get(environment.serverUrl + 'recipes/' + id).pipe(map(this.extractData));
  }

  addRecipe(recipe): Observable<any> {
    return this.httpClient.post<any>(environment.serverUrl + 'recipes', JSON.stringify(recipe), this.httpOptions).pipe(
      tap((response) => console.log(`${recipe.id} has been updated`)),
      catchError(this.handleError<any>('addRecipe'))
    );
  }

  updateRecipe(id, recipe): Observable<any> {
    return this.httpClient.put<any>(environment.serverUrl + 'recipes/' + id, JSON.stringify(recipe), this.httpOptions).pipe(
      tap((response) => console.log(`Recipe with ID=[${id}] has been updated`)),
      catchError(this.handleError<any>('updateRecipe'))
    );
  }

  deleteRecipe(id): Observable<any> {
    return this.httpClient.delete(environment.serverUrl + 'recipes/' + id, this.httpOptions).pipe(
      tap((response) => console.log(`Recipe with ID=[${id}] has been deleted`)),
      catchError(this.handleError<any>('deleteRecipe'))
    );
  }

  getIngredients(): Observable<any> {
    return this.httpClient.get(environment.serverUrl + 'recipes').pipe(map(this.extractData));
  }


  private extractData(res: Response) {
    const body = res;
    return body || { };
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
