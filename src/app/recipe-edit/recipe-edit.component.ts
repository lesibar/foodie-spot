import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendService} from '../service/backend.service';

@Component({
  selector: 'fs-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.scss']
})
export class RecipeEditComponent implements OnInit {

  @Input()
  recipe: any;
  constructor(private router: Router, private restService: BackendService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.restService.getRecipe(this.route.snapshot.params['id']).subscribe(data => {
      console.log('Recipe Data : ', data);
      this.recipe = data;
    });
  }

  updateRecipe() {
    this.restService.updateRecipe(this.route.snapshot.params['id'], this.recipe).subscribe(data => {
      console.log(data);
      this.router.navigate(['/recipe-details/' + data.id]);
    });
  }

}
