import { Component, OnInit } from '@angular/core';
import {BackendService} from '../service/backend.service';

@Component({
  selector: 'fs-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {

  recipes = [];
  constructor(private restService: BackendService) { }

  ngOnInit() {
    this.recipes = [];
    this.restService.getRecipes().subscribe((data) => {
      console.log(`Recipes ${data}`);
      this.recipes = data;
    });
  }
}
