import {Component, Input, OnInit} from '@angular/core';
import {BackendService} from '../service/backend.service';
import {Router} from '@angular/router';

@Component({
  selector: 'fs-recipe-add',
  templateUrl: './recipe-add.component.html',
  styleUrls: ['./recipe-add.component.scss']
})
export class RecipeAddComponent implements OnInit {

  allIngredients = [];

  ingredientTypes = ['ACID_REGULATOR', 'ANT_CAKING_AGENT', 'FOOD_COLOURS', 'FOOD_CULTURE', 'FIRMING_AGENT', 'PERSERVATIVE', 'SWEETENER', 'FLAVOUR_ENHANCER'];

  measurementTypes = ['KILOGRAMS', 'GRAMS', 'MILLILITRES', 'CUPS'];

  currentIngredient: any;

  @Input() recipeData = {
    name: '',
    preparationTime: 0,
    createdDate: new Date(),
    totalCalories: 0,
    ingredients: [],
    dietTypes: [],
    imageUrl: ''
  };

  constructor(private restClient: BackendService, private router: Router) { }

  ngOnInit() {
    this.allIngredients = [];
    this.restClient.getIngredients().subscribe((data) => {
      console.log(`All ingredients: ${data}`);
      this.allIngredients = data;
    });
  }

  createRecipe() {
    this.restClient.addRecipe(this.recipeData).subscribe((data) => {
      this.router.navigate(['/recipe-details/' + data.id]);
    }, (error) => {
      console.log(error);
    });
  }

  addIngredientToRecipe() {
    if (this.currentIngredient) {
      this.recipeData.ingredients.push(this.currentIngredient);
    }
  }

  removeIngredientFromRecipe(id) {
    this.recipeData.ingredients.splice(this.recipeData.ingredients.findIndex(x => x.id === id), 1);
  }

  onSelectChanged(event) {
    this.currentIngredient = event.target.value;
  }

}
